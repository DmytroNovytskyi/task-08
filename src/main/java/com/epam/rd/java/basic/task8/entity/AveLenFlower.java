package com.epam.rd.java.basic.task8.entity;

public class AveLenFlower {
    private final int length;
    private final String measure;

    public AveLenFlower(int length, String measure) {
        this.length = length;
        this.measure = measure;
    }

    public int getLength() {
        return length;
    }

    public String getMeasure() {
        return measure;
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "length=" + length +
                ", measure='" + measure + '\'' +
                '}';
    }
}
