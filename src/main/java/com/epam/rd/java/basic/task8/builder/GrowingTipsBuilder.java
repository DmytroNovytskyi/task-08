package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Lighting;
import com.epam.rd.java.basic.task8.entity.Temperature;
import com.epam.rd.java.basic.task8.entity.Watering;

public class GrowingTipsBuilder {
    private Temperature temperature;
    private Lighting lighting;
    private Watering watering;

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    public GrowingTips getGrowingTips(){
        return new GrowingTips(temperature, lighting, watering);
    }
}
