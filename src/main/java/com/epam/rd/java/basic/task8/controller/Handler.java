package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.builder.FlowerBuilder;
import com.epam.rd.java.basic.task8.builder.GrowingTipsBuilder;
import com.epam.rd.java.basic.task8.builder.VisualParametersBuilder;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.Tags.*;

public class Handler extends DefaultHandler {
    private List<Flower> flowers;
    private String element;
    private String attribute;
    private FlowerBuilder flowerBuilder;
    private VisualParametersBuilder visualParametersBuilder;
    private GrowingTipsBuilder growingTipsBuilder;

    public List<Flower> getFlowers(){
        return flowers;
    }

    @Override
    public void startDocument() {
        flowers = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes){
        switch (qName){
            case FLOWER:
                flowerBuilder = new FlowerBuilder();
                break;
            case VISUAL_PARAMETERS:
                visualParametersBuilder = new VisualParametersBuilder();
                break;
            case GROWING_TIPS:
                growingTipsBuilder = new GrowingTipsBuilder();
                break;
            case AVERAGE_LENGTH_FLOWER:
            case TEMPERATURE:
            case WATERING:
                attribute = attributes.getValue(MEASURE);
                break;
            case LIGHTING:
                attribute = attributes.getValue(LIGHT_REQUIRING);
                break;
            default:
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName){
        switch (qName){
            case NAME:
                flowerBuilder.setName(element);
                break;
            case SOIL:
                flowerBuilder.setSoil(element);
                break;
            case ORIGIN:
                flowerBuilder.setOrigin(element);
                break;
            case STEM_COLOR:
                visualParametersBuilder.setStemColor(element);
                break;
            case LEAF_COLOR:
                visualParametersBuilder.setLeafColor(element);
                break;
            case AVERAGE_LENGTH_FLOWER:
                visualParametersBuilder.setAveLenFlower(new AveLenFlower(Integer.parseInt(element), attribute));
                break;
            case VISUAL_PARAMETERS:
                flowerBuilder.setVisualParameters(visualParametersBuilder.getVisualParameters());
                break;
            case TEMPERATURE:
                growingTipsBuilder.setTemperature(new Temperature(Integer.parseInt(element), attribute));
                break;
            case LIGHTING:
                growingTipsBuilder.setLighting(new Lighting(attribute));
                break;
            case WATERING:
                growingTipsBuilder.setWatering(new Watering(Integer.parseInt(element), attribute));
            break;
            case GROWING_TIPS:
                flowerBuilder.setGrowingTips(growingTipsBuilder.getGrowingTips());
                break;
            case MULTIPLYING:
                flowerBuilder.setMultiplying(element);
                break;
            case FLOWER:
                flowers.add(flowerBuilder.getFlower());
                break;
            default:
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length){
        element = new String(ch, start, length);
    }

}
