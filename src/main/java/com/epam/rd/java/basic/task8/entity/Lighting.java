package com.epam.rd.java.basic.task8.entity;

public class Lighting {
    private final String lightRequired;

    public Lighting(String lightRequired) {
        this.lightRequired = lightRequired;
    }

    public String getLightRequired() {
        return lightRequired;
    }

    @Override
    public String toString() {
        return "Lighting{" +
                "lightRequired='" + lightRequired + '\'' +
                '}';
    }
}
