package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.builder.FlowerBuilder;
import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.Tags.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE

    public List<Flower> readFlowers() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(xmlFileName));
        NodeList list = document.getElementsByTagName(FLOWER);
        List<Flower> flowers = new ArrayList<>();
        for (int i = 0; i < list.getLength(); i++) {
            flowers.add(processFlower(list.item(i)));
        }
        return flowers;
    }

    private Flower processFlower(Node element) {
        FlowerBuilder flowerBuilder = new FlowerBuilder();
        Element flower = (Element) element;
        flowerBuilder.setName(getTagValue(NAME, flower));
        flowerBuilder.setSoil(getTagValue(SOIL, flower));
        flowerBuilder.setOrigin(getTagValue(ORIGIN, flower));
        flowerBuilder.setVisualParameters(processVisualParameters(flower));
        flowerBuilder.setGrowingTips(processGrowingTips(flower));
        flowerBuilder.setMultiplying(getTagValue(MULTIPLYING, flower));
        return flowerBuilder.getFlower();
    }

    private VisualParameters processVisualParameters(Element flower) {
        Element vp = (Element) flower.getElementsByTagName(VISUAL_PARAMETERS).item(0);
        String stemColor = getTagValue(STEM_COLOR, vp);
        String leafColor = getTagValue(LEAF_COLOR, vp);

        int length = Integer.parseInt(getTagValue(AVERAGE_LENGTH_FLOWER, vp));
        String measure = getTagAttributeValue(AVERAGE_LENGTH_FLOWER, MEASURE, vp);
        AveLenFlower aveLenFlower = new AveLenFlower(length, measure);
        return new VisualParameters(stemColor, leafColor, aveLenFlower);
    }

    private GrowingTips processGrowingTips(Element flower) {
        Element gt = (Element) flower.getElementsByTagName(GROWING_TIPS).item(0);

        int degrees = Integer.parseInt(getTagValue(TEMPERATURE, gt));
        String temperatureMeasure = getTagAttributeValue(TEMPERATURE, MEASURE, gt);
        Temperature temperature = new Temperature(degrees, temperatureMeasure);

        String lightRequired = getTagAttributeValue(LIGHTING, LIGHT_REQUIRING, gt);
        Lighting lighting = new Lighting(lightRequired);

        int volume = Integer.parseInt(getTagValue(WATERING, gt));
        String wateringMeasure = getTagAttributeValue(WATERING, MEASURE, gt);
        Watering watering = new Watering(volume, wateringMeasure);

        return new GrowingTips(temperature, lighting, watering);
    }

    private String getTagValue(String tag, Element element) {
        return element.getElementsByTagName(tag).item(0).getChildNodes().item(0).getNodeValue();
    }

    private String getTagAttributeValue(String tag, String attribute, Element element) {
        return element.getElementsByTagName(tag).item(0).getAttributes().getNamedItem(attribute).getNodeValue();
    }

}
