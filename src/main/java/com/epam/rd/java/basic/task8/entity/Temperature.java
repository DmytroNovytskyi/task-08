package com.epam.rd.java.basic.task8.entity;

public class Temperature {
    private final int degrees;
    private final String measure;

    public Temperature(int degrees, String measure) {
        this.degrees = degrees;
        this.measure = measure;
    }

    public int getDegrees() {
        return degrees;
    }

    public String getMeasure() {
        return measure;
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "degrees=" + degrees +
                ", measure='" + measure + '\'' +
                '}';
    }
}
