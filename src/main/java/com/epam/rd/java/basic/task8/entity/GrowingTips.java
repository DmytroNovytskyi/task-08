package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {
    private final Temperature temperature;
    private final Lighting lighting;
    private final Watering watering;

    public GrowingTips(Temperature temperature, Lighting lighting, Watering watering) {
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}
