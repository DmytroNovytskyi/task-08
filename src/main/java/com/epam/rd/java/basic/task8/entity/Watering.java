package com.epam.rd.java.basic.task8.entity;

public class Watering {
    private final int volume;
    private final String measure;

    public Watering(int volume, String measure) {
        this.volume = volume;
        this.measure = measure;
    }

    public int getVolume() {
        return volume;
    }

    public String getMeasure() {
        return measure;
    }

    @Override
    public String toString() {
        return "Watering{" +
                "volume=" + volume +
                ", measure='" + measure + '\'' +
                '}';
    }
}
