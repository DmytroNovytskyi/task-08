package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {
    private final String stemColor;
    private final String leafColor;
    private final AveLenFlower aveLenFlower;

    public VisualParameters(String stemColor, String leafColor, AveLenFlower aveLenFlower) {
        this.stemColor = stemColor;
        this.leafColor = leafColor;
        this.aveLenFlower = aveLenFlower;
    }

    public String getStemColor() {
        return stemColor;
    }

    public String getLeafColor() {
        return leafColor;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColor='" + stemColor + '\'' +
                ", leafColor='" + leafColor + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }
}
