package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.entity.AveLenFlower;
import com.epam.rd.java.basic.task8.entity.VisualParameters;

public class VisualParametersBuilder {
    private String stemColor;
    private String leafColor;
    private AveLenFlower aveLenFlower;

    public void setStemColor(String stemColor) {
        this.stemColor = stemColor;
    }

    public void setLeafColor(String leafColor) {
        this.leafColor = leafColor;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public VisualParameters getVisualParameters(){
        return new VisualParameters(stemColor, leafColor, aveLenFlower);
    }
}
