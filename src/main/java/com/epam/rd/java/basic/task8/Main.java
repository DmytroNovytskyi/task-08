package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import static com.epam.rd.java.basic.task8.Tags.*;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        // PLACE YOUR CODE HERE
        List<Flower> domFlowers = domController.readFlowers();
        // sort (case 1)
        // PLACE YOUR CODE HERE
        sortByName(domFlowers);

        // save
        String outputXmlFile = "output.dom.xml";
        // PLACE YOUR CODE HERE
        writeFlowers(outputXmlFile, domFlowers);
        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        List<Flower> saxFlowers = saxController.readFlowers();
        // sort  (case 2)
        // PLACE YOUR CODE HERE
        sortBySoil(saxFlowers);
        // save
        outputXmlFile = "output.sax.xml";
        // PLACE YOUR CODE HERE
        writeFlowers(outputXmlFile, saxFlowers);
        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        // PLACE YOUR CODE HERE
        List<Flower> staxFlowers = staxController.readFlowers();
        // sort  (case 3)
        // PLACE YOUR CODE HERE
        sortByOrigin(staxFlowers);
        // save
        outputXmlFile = "output.stax.xml";
        // PLACE YOUR CODE HERE
        writeFlowers(outputXmlFile, staxFlowers);
    }

    private static void sortByName(List<Flower> flowers) {
        flowers.sort(Comparator.comparing(Flower::getName));
    }

    private static void sortBySoil(List<Flower> flowers) {
        flowers.sort(Comparator.comparing(Flower::getSoil));
    }

    private static void sortByOrigin(List<Flower> flowers) {
        flowers.sort(Comparator.comparing(Flower::getOrigin));
    }

    public static void writeFlowers(String outputXmlFile, List<Flower> flowers) throws IOException {
        try (FileWriter fileWriter = new FileWriter(outputXmlFile)) {
            fileWriter.write("<" + FLOWERS + " xmlns=\"http://www.nure.ua\" " +
                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" +
                    " xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n");
            for (Flower flower :
                    flowers) {
                printFlower(fileWriter, flower);
            }
            fileWriter.write("</" + FLOWERS + ">");
        }
    }

    private static void printFlower(FileWriter fileWriter, Flower flower) throws IOException {
        fileWriter.write("<" + FLOWER + ">\n");
        printTagValue(fileWriter, NAME, flower.getName());
        printTagValue(fileWriter, SOIL, flower.getSoil());
        printTagValue(fileWriter, ORIGIN, flower.getOrigin());
        fileWriter.write("<" + VISUAL_PARAMETERS + ">\n");
        VisualParameters vp = flower.getVisualParameters();
        printTagValue(fileWriter, STEM_COLOR, vp.getStemColor());
        printTagValue(fileWriter, LEAF_COLOR, vp.getLeafColor());
        AveLenFlower avf = vp.getAveLenFlower();
        if (avf != null) {
            printTagAttributeValue(fileWriter, AVERAGE_LENGTH_FLOWER, MEASURE, avf.getMeasure(), String.valueOf(avf.getLength()));
        }
        fileWriter.write("</" + VISUAL_PARAMETERS + ">\n");
        fileWriter.write("<" + GROWING_TIPS + ">\n");
        GrowingTips gt = flower.getGrowingTips();
        Temperature tmp = gt.getTemperature();
        printTagAttributeValue(fileWriter, TEMPERATURE, MEASURE, tmp.getMeasure(), String.valueOf(tmp.getDegrees()));
        Lighting lt = gt.getLighting();
        printTagAttribute(fileWriter, LIGHTING, LIGHT_REQUIRING, lt.getLightRequired());
        Watering wt = gt.getWatering();
        if (wt != null) {
            printTagAttributeValue(fileWriter, WATERING, MEASURE, wt.getMeasure(), String.valueOf(wt.getVolume()));
        }
        fileWriter.write("</" + GROWING_TIPS + ">\n");
        printTagValue(fileWriter, MULTIPLYING, flower.getMultiplying());
        fileWriter.write("</" + FLOWER + ">\n");
    }

    private static void printTagValue(FileWriter fileWriter, String tag, String tagValue) throws IOException {
        fileWriter.write("<" + tag + ">" + tagValue + "</" + tag + ">\n");
    }

    private static void printTagAttributeValue(FileWriter fileWriter, String tag, String attribute, String attributeValue, String tagValue) throws IOException {
        fileWriter.write("<" + tag + " " + attribute + "=\"" + attributeValue + "\">" + tagValue + "</" + tag + ">\n");
    }

    private static void printTagAttribute(FileWriter fileWriter, String tag, String attribute, String attributeValue) throws IOException {
        fileWriter.write("<" + tag + " " + attribute + "=\"" + attributeValue + "\"/>\n");
    }
}
