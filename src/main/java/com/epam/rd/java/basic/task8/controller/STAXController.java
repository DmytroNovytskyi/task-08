package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.builder.FlowerBuilder;
import com.epam.rd.java.basic.task8.builder.GrowingTipsBuilder;
import com.epam.rd.java.basic.task8.builder.VisualParametersBuilder;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.Tags.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    public List<Flower> readFlowers() throws FileNotFoundException, XMLStreamException {
        List<Flower> flowers = new ArrayList<>();
        String element = "";
        String attribute = "";
        FlowerBuilder flowerBuilder = new FlowerBuilder();
        VisualParametersBuilder visualParametersBuilder = new VisualParametersBuilder();
        GrowingTipsBuilder growingTipsBuilder = new GrowingTipsBuilder();
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
        while (reader.hasNext()) {
            XMLEvent nextEvent = reader.nextEvent();
            if (nextEvent.isStartDocument()) {
                flowers = new ArrayList<>();
            } else if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();
                switch (startElement.getName().getLocalPart()) {
                    case FLOWER:
                        flowerBuilder = new FlowerBuilder();
                        break;
                    case VISUAL_PARAMETERS:
                        visualParametersBuilder = new VisualParametersBuilder();
                        break;
                    case GROWING_TIPS:
                        growingTipsBuilder = new GrowingTipsBuilder();
                        break;
                    case AVERAGE_LENGTH_FLOWER:
                    case TEMPERATURE:
                    case WATERING:
                        attribute = startElement.getAttributeByName(new QName(MEASURE)).getValue();
                        break;
                    case LIGHTING:
                        attribute = startElement.getAttributeByName(new QName(LIGHT_REQUIRING)).getValue();
                        break;
                    default:
                        break;
                }
            } else if (nextEvent.isCharacters()) {
                element = nextEvent.asCharacters().getData();
            } else if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                switch (endElement.getName().getLocalPart()) {
                    case NAME:
                        flowerBuilder.setName(element);
                        break;
                    case SOIL:
                        flowerBuilder.setSoil(element);
                        break;
                    case ORIGIN:
                        flowerBuilder.setOrigin(element);
                        break;
                    case STEM_COLOR:
                        visualParametersBuilder.setStemColor(element);
                        break;
                    case LEAF_COLOR:
                        visualParametersBuilder.setLeafColor(element);
                        break;
                    case AVERAGE_LENGTH_FLOWER:
                        visualParametersBuilder.setAveLenFlower(new AveLenFlower(Integer.parseInt(element), attribute));
                        break;
                    case VISUAL_PARAMETERS:
                        flowerBuilder.setVisualParameters(visualParametersBuilder.getVisualParameters());
                        break;
                    case TEMPERATURE:
                        growingTipsBuilder.setTemperature(new Temperature(Integer.parseInt(element), attribute));
                        break;
                    case LIGHTING:
                        growingTipsBuilder.setLighting(new Lighting(attribute));
                        break;
                    case WATERING:
                        growingTipsBuilder.setWatering(new Watering(Integer.parseInt(element), attribute));
                        break;
                    case GROWING_TIPS:
                        flowerBuilder.setGrowingTips(growingTipsBuilder.getGrowingTips());
                        break;
                    case MULTIPLYING:
                        flowerBuilder.setMultiplying(element);
                        break;
                    case FLOWER:
                        flowers.add(flowerBuilder.getFlower());
                        break;
                    default:
                        break;
                }
            }
        }
        return flowers;
    }
}